import os

from aws_cdk import (
    aws_dynamodb as dynamodb,
    core as cdk,
    aws_sqs as sqs,
    aws_iam as iam,
    aws_cloudwatch as cloudwatch,
    aws_apigateway as apigateway,
)
from chalice.cdk import Chalice


RUNTIME_SOURCE_DIR = os.path.join(
    os.path.dirname(os.path.dirname(__file__)), os.pardir, 'runtime')


class ChaliceApp(cdk.Stack):

    def __init__(self, scope, id, **kwargs):
        super().__init__(scope, id, **kwargs)
        self.dynamodb_table = self._create_ddb_table()
        self.the_queue = self._create_queue()

        self.chalice = Chalice(
            self, 
            'ChaliceApp', 
            source_dir=RUNTIME_SOURCE_DIR,
            stage_config={
                'environment_variables': {
                    'APP_TABLE_NAME': self.dynamodb_table.table_name,
                    'QUEUE_NAME': self.the_queue.queue_name,
                }
            },
        )

        chaliceRole = self.chalice.get_role('DefaultRole')
        self.dynamodb_table.grant_read_write_data(chaliceRole)
        self.the_queue.grant_consume_messages(chaliceRole)
        self.the_queue.grant_send_messages(chaliceRole)

        self.restApi = self.chalice.get_resource('RestAPI')
        self.restApiDimension = self.restApi.definition_body['info']['title']

        self._api_latency_alarm = cloudwatch.Alarm(
            self,
            'APILatencyTooHigh',
            metric=cloudwatch.Metric(
                namespace='AWS/ApiGateway',
                metric_name='Latency',
                dimensions={'ApiName': self.restApiDimension},
            ),
            threshold=1000,
            evaluation_periods=3,
            datapoints_to_alarm=2
        )

        self._api_errors = cloudwatch.Alarm(
            self,
            'API Errors',
            metric=cloudwatch.Metric(
                namespace='AWS/ApiGateway',
                metric_name='5XXError',
                dimensions={'ApiName': self.restApiDimension},
            ),
            threshold=1,
            evaluation_periods=3,
            datapoints_to_alarm=2
        )

        self.dashboard = self._create_dashboard()
        

    def _create_queue(self) -> sqs.Queue:
        the_queue = sqs.Queue(
            self, 'the_queue',
            visibility_timeout=cdk.Duration.seconds(60),
        )
        cdk.CfnOutput(self, 'QueueName', value=the_queue.queue_name)

        self._queue_alarm = cloudwatch.Alarm(
            self,
            'TheQueueHasFailingItems',
            metric=the_queue.metric('ApproximateAgeOfOldestMessage'),
            threshold=100,
            evaluation_periods=3,
            datapoints_to_alarm=2
        )

        return the_queue

    def _create_ddb_table(self) -> dynamodb.Table:
        dynamodb_table = dynamodb.Table(
            self,
            'AppTable',
            partition_key=dynamodb.Attribute(name='PK', type=dynamodb.AttributeType.STRING),
            sort_key=dynamodb.Attribute(name='SK', type=dynamodb.AttributeType.STRING),
            removal_policy=cdk.RemovalPolicy.DESTROY,
        )

        cdk.CfnOutput(self, 'AppTableName', value=dynamodb_table.table_name)
        return dynamodb_table

    def _create_dashboard(self) -> cloudwatch.Dashboard:
        db = cloudwatch.Dashboard(
            self, 
            'Dashboard',
            start='-P1D',
        )

        db.add_widgets(
            cloudwatch.AlarmStatusWidget(
                title='Alarms',
                alarms=[
                    self._queue_alarm,
                    self._api_latency_alarm,
                    self._api_errors,
                ],
            ),
            cloudwatch.AlarmWidget(
                title='Queue Latency',
                alarm=self._queue_alarm,
            ),
            cloudwatch.AlarmWidget(
                title='API Latency',
                alarm=self._api_latency_alarm,
            ),
            cloudwatch.GraphWidget(
                title='API Errors',
                left=[
                    cloudwatch.Metric(
                        namespace='AWS/ApiGateway',
                        metric_name='5XXError',
                        dimensions={'ApiName': self.restApiDimension},
                    ),
                    cloudwatch.Metric(
                        namespace='AWS/ApiGateway',
                        metric_name='4XXError',
                        dimensions={'ApiName': self.restApiDimension},
                    ),
                ],
            ),
        )

        # cdk.CfnOutput(self, 'DashboardUrl', value=db.)

        return db


