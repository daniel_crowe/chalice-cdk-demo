import datetime
import logging
import os
import boto3
from chalice import Chalice


app = Chalice(app_name='dcrowe-chalice-api-play-1')
app.log.setLevel(logging.DEBUG)


dynamodb = boto3.resource('dynamodb')
dynamodb_table = dynamodb.Table(os.environ.get('APP_TABLE_NAME', ''))
sqs = boto3.resource('sqs')


@app.route('/users', methods=['GET'])
def get_all_users():
    app.log.info('get_all_users')
    return dynamodb_table.scan()['Items']


@app.route('/environ', methods=['GET'])
def get_all_environ():
    return dict(os.environ)


@app.route('/users', methods=['POST'])
def create_user():
    request = app.current_request.json_body
    item = {
        'PK': 'User#%s' % request['username'],
        'SK': 'Profile#%s' % request['username'],
        'Updated': datetime.datetime.now().isoformat(),
    }
    item.update(request)
    dynamodb_table.put_item(Item=item)

    the_queue = sqs.get_queue_by_name(QueueName=os.environ.get('QUEUE_NAME', ''))
    the_queue.send_message(
        MessageBody=request['username'],
    )

    return item


@app.route('/users/{username}', methods=['GET'])
def get_user(username):
    key = {
        'PK': 'User#%s' % username,
        'SK': 'Profile#%s' % username,
    }
    item = dynamodb_table.get_item(Key=key)['Item']
    del item['PK']
    del item['SK']
    return item


# @app.on_sqs_message(queue=os.environ.get('QUEUE_NAME', ''))
@app.on_sqs_message(queue='dcrowe-chalice-api-play-1-thequeue049060B3-1GX30CZ5J5JLP')
def the_queue_handler(event):
    username = list(event)[0].body
    app.log.info('the_queue_handler', username)

    dynamodb_table.update_item(
        Key={
            'PK': 'User#%s' % username,
            'SK': 'Profile#%s' % username,
        },
        UpdateExpression='SET QueueHandled = :QueueHandled',
        ExpressionAttributeValues={
            ':QueueHandled': datetime.datetime.now().isoformat(),
        }
    )
